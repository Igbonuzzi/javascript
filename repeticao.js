let numero = 0
while (numero <= 10) {
    if (numero % 2 == 0) {
        console.log(`Valor nr: ${numero} é par`)
    } else {
        console.log(`Valor nr: ${numero} é ímpar`)
    }
    numero++
}

console.log("\nREPETIÇÃO DO/WHILE\n")

let numero1 = 0
do {
    console.log(`Valor nr: ${numero1}`)
    numero1++
} while (numero1 <= 10)

console.log("\nREPETIÇÃO FOR\n")

let numero2 = 0
for (numero2 = 0; numero2 <= 10; numero2++) {
    console.log(`Valor nr: ${numero2}`)
}